﻿using System;

namespace MicrowaveOvenController
{
    public class MicrowaveOvenController : IMicrowaveOvenHw
    {
        public event Action<bool> DoorOpenChanged;
        public event EventHandler StartButtonPressed;

        public bool DoorOpen { get; private set; }

        private readonly IHeater _heater;       

        private int PressedCounter { get; set; }

        public MicrowaveOvenController(
            IDoor door, 
            ILight light,
            IStartButton startButton, 
            IHeater heater)
        {
            _heater = heater;

            door.DoorOpenChanged = s =>
            {
                if (DoorOpen == s) return;
                DoorOpen = s;
                light.SwitchOnOff(s);
                DoorOpenChanged?.Invoke(s);                
            };

            startButton.Pressed = () =>
            {
                if(DoorOpen == false)
                    StartButtonPressed?.Invoke(this, null);
            };

            StartButtonPressed += HandleStartButtonPressed;
            DoorOpenChanged += HandleDoorOpenChanged;
        }

        public void TurnOnHeater()
        {
            const int onePressed = 1;

            if (PressedCounter == onePressed)
                _heater.IncreaseTime();
            else
            {
                PressedCounter = onePressed;
                _heater.StartHeat();
            }           
        }

        public void TurnOffHeater()
        {
            PressedCounter = 0;
            _heater.StopHeat();
        }

        private void HandleDoorOpenChanged(bool status)
        {
            if (DoorOpen && _heater.IsHeat)
                TurnOffHeater();
        }

        private void HandleStartButtonPressed(object o, EventArgs e)
        {
            if (!DoorOpen)
                TurnOnHeater();
        }
    }
}