﻿
namespace MicrowaveOvenController
{
    public class Light : ILight
    {
        public bool IsLight { get; private set; }

        public void SwitchOnOff(bool state)
        {
            IsLight = state;
        }
    }
}
