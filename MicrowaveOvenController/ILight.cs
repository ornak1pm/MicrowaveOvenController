﻿namespace MicrowaveOvenController
{
    public interface ILight
    {
        bool IsLight { get; }

        void SwitchOnOff(bool state);
    }
}