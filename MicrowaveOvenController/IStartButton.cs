﻿using System;

namespace MicrowaveOvenController
{
    public interface IStartButton
    {
        Action Pressed { get; set; }
    }
}