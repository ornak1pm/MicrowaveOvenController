﻿using System;

namespace MicrowaveOvenController
{
    public class StartButton : IStartButton
    {
        public Action Pressed { get; set; }
    }
}
