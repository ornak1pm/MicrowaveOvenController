﻿namespace MicrowaveOvenController
{
    public interface IHeater
    {
        bool IsHeat { get; }
        int Time { get; }

        void IncreaseTime();
        void StartHeat();
        void StopHeat();
    }
}