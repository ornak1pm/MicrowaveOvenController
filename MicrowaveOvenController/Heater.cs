﻿using System;
using System.Timers;

namespace MicrowaveOvenController
{
    public class Heater : IHeater
    {
        private const ushort Second = 1000;
        private const ushort HeatTimeUnit = 60 * Second;

        private readonly Timer _timer;
        private DateTime _startTime;

        public bool IsHeat { get; private set; }
        public int Time => _timer != null ? (int)_timer.Interval : 0;
     
        public Heater()
        {
            _timer = new Timer();
            _timer.Elapsed += (o, e) => StopHeat();
        }

        public void StartHeat()
        {
            if (IsHeat) return;
            IsHeat = true;

            _timer.Interval = HeatTimeUnit;
            _timer.Start();
            _startTime = DateTime.Now;            
        }

        public void StopHeat()
        {            
            _timer.Stop();
            IsHeat = false;
        }

        public void IncreaseTime()
        {
            var newInterval = 2 * HeatTimeUnit - (DateTime.Now - _startTime).TotalMilliseconds;
            if (newInterval > 0)
                _timer.Interval = newInterval;
            else
                StopHeat();
        }
    }
}
