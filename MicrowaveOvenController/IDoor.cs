﻿using System;

namespace MicrowaveOvenController
{
    public interface IDoor
    {
        Action<bool> DoorOpenChanged { get; set; }
    }
}