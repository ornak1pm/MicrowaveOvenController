﻿using System;

namespace MicrowaveOvenController
{
    public class Door : IDoor
    {
        public Action<bool> DoorOpenChanged { get; set; }
    }
}
