﻿Feature: Microwave Oven Controler

@MicrowaveOvenHW

Scenario: When I open door Light is on.
	Given The door is closed
	When  I open door
	Then  The light will be on

Scenario: When I close door Light turns off.
	Given The door is opened
	When  I close door
	Then  The light will be off

Scenario: When I open door heater stops if running.
	Given The heater is running
	When  I open door
	Then  The heater will be stopped

Scenario: When I press start button when door is open nothing happens.
	Given The door is opened
	When  I press start button
	Then  The heater will be still stopped

Scenario: When I press start button when door is closed, heater runs for one minute.
	Given The door is closed
	When  I press start button
	Then  The heater will be run for one minute

Scenario: When I press start button when door is closed and already heating, 
		  increase remaining time with 1 minute.
	Given The door is closed
	 And  The heater is running
	When  I press start button
	Then  The remaining time will be increased with one minute