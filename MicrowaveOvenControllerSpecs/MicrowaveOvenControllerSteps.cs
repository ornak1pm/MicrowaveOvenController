﻿using System;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MicrowaveOvenController;
using TechTalk.SpecFlow;

namespace MicrowaveOvenControllerSpecs
{
    [Binding]
    public sealed class MicrowaveOvenControllerSteps
    {
        private IMicrowaveOvenHw _microwaveOvenController;
        private Door _door;
        private Light _light;
        private StartButton _startButton;
        private Heater _heater;

        private const int Second = 1000; //milliseconds
        private const int OneMinute = 60 * Second; //milliseconds

        private DateTime _startTime = DateTime.Now;
        private const int Delay = 200; //milliseconds

        [BeforeScenario]
        public void Init()
        {
            _door = new Door();
            _light = new Light();
            _startButton = new StartButton();
            _heater = new Heater();
            _microwaveOvenController = new MicrowaveOvenController.MicrowaveOvenController(_door, _light, _startButton, _heater);            
        }

        [Given(@"The door is opened")]
        [When(@"I open door")]
        public void WhenIOpenDoor()
        {
            _door?.DoorOpenChanged(true);
            Assert.AreEqual(_microwaveOvenController.DoorOpen, true);
        }

        [Given(@"The door is closed")]
        [When(@"I close door")]        
        public void GivenTheDoorIsClosed()
        {
            _door?.DoorOpenChanged(false);
            Assert.AreEqual(_microwaveOvenController.DoorOpen, false);
        }

        [Then(@"The light will be on")]
        public void ThenTheLightWillBeOn()
        {
            Assert.AreEqual(_light.IsLight, true);
        }

        [Then(@"The light will be off")]
        public void ThenTheLightWillBeOff()
        {
            Assert.AreEqual(_light.IsLight, false);
        }

        [Given(@"The heater is running")]
        public void GivenTheHeaterIsRunning()
        {
            _door?.DoorOpenChanged(false);
            _startButton?.Pressed();
            Assert.AreEqual(_heater.IsHeat, true);
        }

        [Then(@"The heater will be stopped")]
        [Then(@"The heater will be still stopped")]
        public void ThenTheHeaterWillBeStopped()
        {
            Assert.AreEqual(_heater.IsHeat, false);
        }

        [When(@"I press start button")]
        public void WhenIPressStartButton()
        {
            _startButton?.Pressed();
        }

        [Then(@"The heater will be run for one minute")]
        public void ThenTheHeaterWillBeRunForMinute()
        {
            _startTime = DateTime.Now;

            Assert.AreEqual(_heater.Time == OneMinute, true);
           
            while (_heater.IsHeat && (DateTime.Now - _startTime).TotalMilliseconds < OneMinute + Delay) ;
            if (!_heater.IsHeat)
                Assert.AreEqual(Enumerable.Range(OneMinute - Delay, OneMinute)
                    .Contains((int)(DateTime.Now - _startTime).TotalMilliseconds), true);
            else
                Assert.Fail("The heater hasn't been stopped");            
        }

        [Then(@"The remaining time will be increased with one minute")]
        public void ThenTheRemainingTimeWillBeIncreasedWithMinute()
        {
            _startTime = DateTime.Now;

            const int twoMinutes = 2 * OneMinute;
            Assert.AreEqual(Enumerable.Range(twoMinutes - Delay, twoMinutes).Contains(_heater.Time), true);            

            while (_heater.IsHeat && (DateTime.Now - _startTime).TotalMilliseconds < twoMinutes + Delay) ;
            if (!_heater.IsHeat)
                Assert.AreEqual(Enumerable.Range(twoMinutes - Delay, twoMinutes)
                    .Contains((int)(DateTime.Now - _startTime).TotalMilliseconds), true);
            else
                Assert.Fail("The heater hasn't been stopped");
        }
    }
}
