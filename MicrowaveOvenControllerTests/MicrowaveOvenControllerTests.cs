﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using MicrowaveOvenController;
using Moq;

namespace MicrowaveOvenControllerTests
{
    [TestClass]
    public class MicrowaveOvenControllerTests
    {
        private Mock<IDoor> _mockDoor;
        private Mock<ILight> _mockLight;
        private Mock<IStartButton> _mockStartButton;
        private Mock<IHeater> _mockHeater;

        [TestInitialize]
        public void Init()
        {
            _mockLight = new Mock<ILight>();
            _mockDoor = new Mock<IDoor>();
            _mockStartButton = new Mock<IStartButton>();
            _mockHeater = new Mock<IHeater>();
        }

        [TestMethod]
        public void ShouldLightSwitchOnAfterOpenedDoor()
        {
            _mockLight.Setup(m => m.SwitchOnOff(true));
            var door = new Door();
            var startButton = new StartButton();
            var mocMicrowaveOvenController = new MicrowaveOvenController.MicrowaveOvenController(
                door, _mockLight.Object, _mockStartButton.Object, _mockHeater.Object);

            door.DoorOpenChanged(true);

            _mockLight.Verify(m => m.SwitchOnOff(true), Times.Once());
        }

        [TestMethod]
        public void ShouldLightSwitchOffAfterClosedDoor()
        {
            _mockLight.Setup(m => m.SwitchOnOff(false));
            var door = new Door();
            var startButton = new StartButton();
            var mocMicrowaveOvenController = new MicrowaveOvenController.MicrowaveOvenController(
                door, _mockLight.Object, _mockStartButton.Object, _mockHeater.Object);
            door.DoorOpenChanged(true);

            door.DoorOpenChanged(false);

            _mockLight.Verify(m => m.SwitchOnOff(false), Times.Once());
        }

        [TestMethod]
        public void ShouldStoptHeatAfterOpenedDoor()
        {
            _mockHeater.Setup(m => m.StopHeat());
            _mockHeater.Setup(m => m.IsHeat).Returns(true);
            var door = new Door();
            var startButton = new StartButton();
            var mocMicrowaveOvenController = new MicrowaveOvenController.MicrowaveOvenController(
                door, _mockLight.Object, startButton, _mockHeater.Object);
            startButton.Pressed();

            door.DoorOpenChanged(true);

            _mockHeater.Verify(m => m.StopHeat(), Times.Once());
        }

        [TestMethod]
        public void ShouldNothingHappenAfterPressingStartButtonIfDoorIsOpened()
        {
            _mockHeater.Setup(m => m.StartHeat());
            var startButton = new StartButton();
            var door = new Door();
            var mocMicrowaveOvenController = new MicrowaveOvenController.MicrowaveOvenController(
                door, _mockLight.Object, startButton, _mockHeater.Object);
            door.DoorOpenChanged(true);

            startButton.Pressed();

            _mockHeater.Verify(m => m.StartHeat(), Times.Never);
        }

        [TestMethod]
        public void ShouldStartHeatAfterPressedButton()
        {
            _mockHeater.Setup(m => m.StartHeat());
            var startButton = new StartButton();
            var mocMicrowaveOvenController = new MicrowaveOvenController.MicrowaveOvenController(
                _mockDoor.Object, _mockLight.Object, startButton, _mockHeater.Object);

            startButton.Pressed();

            _mockHeater.Verify(m => m.StartHeat(), Times.Once());
        }

        [TestMethod]
        public void ShouldIncreaseTimeoutAfterPressedButtonTwice()
        {
            _mockHeater.Setup(m => m.IncreaseTime());
            var startButton = new StartButton();
            var mocMicrowaveOvenController = new MicrowaveOvenController.MicrowaveOvenController(
                _mockDoor.Object, _mockLight.Object, startButton, _mockHeater.Object);

            startButton.Pressed();
            startButton.Pressed();

            _mockHeater.Verify(m => m.IncreaseTime(), Times.Once());
        }
    }
}
